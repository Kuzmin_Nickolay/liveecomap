﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Models.Entities;

namespace LiveEcoMap.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }
        public DbSet<EcoUser> EcoUsers { get; set; }
        public DbSet<Marker> Markers { get; set; }
        public DbSet<MarkerCategory> MarkerCategories { get; set; }
        public DbSet<MarkerState> MarkerStates { get; set; }
        public DbSet<MarkerUser> MarkerUsers { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<MarkerUser>()
                .HasKey(mu => new { mu.MarkerId, mu.UserId });

            modelBuilder.Entity<MarkerUser>()
                .HasOne(mu => mu.Marker)
                .WithMany(m => m.MarkerUsers)
                .HasForeignKey(mu => mu.MarkerId);

            modelBuilder.Entity<MarkerUser>()
                .HasOne(mu => mu.EcoUser)
                .WithMany(u => u.MarkerUsers)
                .HasForeignKey(mu => mu.UserId);
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
            => optionsBuilder
                .UseLazyLoadingProxies()
                .UseSqlServer("Server=localhost\\SQLEXPRESS;Database=master;Trusted_Connection=True;");
    }
}
