﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Models.Entities
{
    public class Marker
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public float PointLatitude { get; set; }
        public float PointLongitude { get; set; }


        public virtual MarkerCategory MarkerCategory { get; set; }
        public int MarkerCategoryId { get; set; }

        public int MarkerStateId { get; set; }
        public virtual MarkerState MarkerState { get; set; }

        public int OwnerId { get; set; }
        public virtual EcoUser Owner { get; set; }

        public virtual List<MarkerUser> MarkerUsers { get; set; }
    }
}
