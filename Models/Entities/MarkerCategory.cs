﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Models.Entities
{
    public class MarkerCategory
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string IconPath { get; set; }
    }
}
